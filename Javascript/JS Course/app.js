/* -------- Console ------- */
/* console.log("Hello world!");
console.error("This is an error");
console.warn("Warning!!!")  */


/* ----- Variable ------ */
const number = 30; // constant variable (the value be cannot re-assign)
let score = 400;
score = 100;
var name1 = "Dummy";

console.log(number, score, name1); 


/* ------ Javascript primitiva data types (String, number, boolean, undefined, null) -----  */
const name2 = "John";
const age = 30;
const isCool = true;
const y = null;
const x = undefined;


/* ------ STRINGS ------- */

// concatenation
console.log('My name is ' + name2 + 'and my age is ' + age);
//Template literal.
console.log(`My name is ${name2} and i am ${30} years old.`);

// String method and properties

const string1 = "Hello World!"

let val;

// Get length
val = string1.length;
console.log(val);
// Change case
val = string1.toUpperCase();
console.log(val);
val = string1.toLowerCase();
console.log(val);
// Get sub string
val = string1.substring(0, 5);
console.log(val);
// Split into array
val = string1.split(' ');
console.log(val);

/* ---------- ARRAYS - Store multiple values in a variable ---------- */
const numbers = [1,2,3,4,5];
const animals = ['fish', 'cat', 'dog', 'bird'];
console.log(numbers, animals);

// Get one value - Arrays start at 0
console.log(animals[1]);

// Add value
animals[4] = 't-rex';

// Add value using push()
animals.push('cow');

// Add to beginning
animals.unshift('shark');

// Remove last value
animals.pop();

// Check if array
console.log(Array.isArray(animals));

// Get index
console.log(animals.indexOf('cat'));

/* ---------- OBJECT LITERALS ---------- */
const person = {
    firstName: 'Ifan',
    age: 30,
    hobbies: ['video games', 'movies', 'sports'],
    address: {
      street: '50 Main st',
      city: 'Boston',
      state: 'MA'
    }
  }
  
// Get single value
console.log(person.name)

// Get array value
console.log(person.hobbies[1]);

// Get embedded object
console.log(person.address.city);

// Add property
person.email = 'ifan@gmail.com';

// Array of objects
const todos = [
{
    id: 1,
    text: 'Take a bath',
    isComplete: false
},
{
    id: 2,
    text: 'Breakfast',
    isComplete: false
},
{
    id: 3,
    text: 'Go to work',
    isComplete: true
}
];

// Get specific object value
console.log(todos[1].text);

// Format as JSON
console.log(JSON.stringify(todos));


/* ---------- LOOPS ---------- */

// For
for(let i = 0; i <= 10; i++){
console.log(`For Loop Number: ${i}`);
}

// While
let i = 0
while(i <= 10) {
console.log(`While Loop Number: ${i}`);
i++;
}

// Loop Through Arrays
// For Loop
for(let i = 0; i < todos.length; i++){
console.log(` Todo ${i + 1}: ${todos[i].text}`);
}

// For...of Loop
for(let todo of todos) {
console.log(todo.text);
}


/* ---------- HIGH ORDER ARRAY METHODS (show prototype) ---------- */

// forEach() - Loops through array
todos.forEach(function(todo, i, myTodos) {
console.log(`${i + 1}: ${todo.text}`);
console.log(myTodos);
});

// map() - Loop through and create new array
const todoTextArray = todos.map(function(todo) {
return todo.text;
});

console.log(todoTextArray);

// filter() - Returns array based on condition
const todo1 = todos.filter(function(todo) {
// Return only todos where id is 1
return todo.id === 1; 
});

/* ------- CONDITIONALS -------- */

// Simple If/Else Statement
const number2 = 30;

if(number2 === 10) {
  console.log('number2 is 10');
} else if(number2 > 10) {
  console.log('number2 is greater than 10');
} else {
  console.log('number2 is less than 10')
}

// Switch
color = 'blue';

switch(color) {
  case 'red':
    console.log('color is red');
  case 'blue':
    console.log('color is blue');
  default:  
    console.log('color is not red or blue')
}

// Ternary operator / Shorthand if
const z = color === 'red' ? 10 : 20;

/* OOP ------- */

// Constructor Function
function Person(firstName, lastName, dob) {
    // Set object properties
    this.firstName = firstName;
    this.lastName = lastName;
    this.dob = new Date(dob); // Set to actual date object using Date constructor
    // this.getBirthYear = function(){
    //   return this.dob.getFullYear();
    // }
    // this.getFullName = function() {
    //   return `${this.firstName} ${this.lastName}`
    // }
  }
  
  // Get Birth Year
  Person.prototype.getBirthYear = function () {
    return this.dob.getFullYear();
  }
  
  // Get Full Name
  Person.prototype.getFullName = function() {
    return `${this.firstName} ${this.lastName}`
  }
  
  
  // Instantiate an object from the class
  const data1 = new Person('John', 'Doe', '7-8-80');
  const data2 = new Person('Steve', 'Smith', '8-2-90');
  
  console.log(data1);
  
  // console.log(person1.getBirthYear());
  // console.log(person1.getFullName());

  // Built in constructors
const name = new String('Kevin');
console.log(typeof name); // Shows 'Object'
const num = new Number(5);
console.log(typeof num); // Shows 'Object'


// ES6 CLASSES
class bioData {
  constructor(firstName, lastName, dob) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.dob = new Date(dob);
  }

  // Get Birth Year
  getBirthYear() {
    return this.dob.getFullYear();
  }

  // Get Full Name
  getFullName() {
    return `${this.firstName} ${this.lastName}`
  }
}

const person1 = new bioData('John', 'Doe', '7-8-80');
console.log(person1.getBirthYear());
