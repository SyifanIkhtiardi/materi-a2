package main

import (
	"fmt"
	"sync"
	"time"
)

var conferenceName = "Go Conference"

const conferenceTicket int = 50

var remainingTicket uint = 50
var bookings = make([]UserData, 0)

type UserData struct {
	firstName       string
	lastName        string
	email           string
	numbersOfTicket uint
}

var wg = sync.WaitGroup{}

func main() {

	greetUser()

	firstName, lastName, email, userTicket := getUserInput()
	isValidName, isValidEmail, isValidTicketNumber := ValidateUserIput(firstName, lastName, email, userTicket)

	if isValidName && isValidEmail && isValidTicketNumber {
		bookTicket(userTicket, firstName, lastName, email)

		wg.Add(1)
		go sendTicket(userTicket, firstName, lastName, email)

		// Call function get firstname
		firstNames := getFirstName()
		fmt.Printf("The first name of our bookings are: %v\n", firstNames)

		if remainingTicket == 0 {
			// End process
			fmt.Println("Our conference tickets are sold out. Please comeback next year")
			//break
		}
	} else {
		if isValidName {
			fmt.Println("Your first name or your last name is too short.")
		}
		if isValidEmail {
			fmt.Println("Your email address doesn't contain @ symbol.")
		}
		if isValidTicketNumber {
			fmt.Println("Number of tickets you entered is invalid.")
		}
	}
	wg.Wait()
}

func greetUser() {
	fmt.Printf("Welcome to our %v booking application\n", conferenceName)
	fmt.Printf("We have total of %v tickets and %v are  still available\n", conferenceTicket, remainingTicket)
	fmt.Println("Get your ticket here to attend")
}

func getFirstName() []string {
	firstNames := []string{}
	for _, booking := range bookings {
		firstNames = append(firstNames, booking.firstName)
	}
	return firstNames
}

func getUserInput() (string, string, string, uint) {
	var firstName string
	var lastName string
	var email string
	var userTicket uint
	fmt.Println("Enter your first name: ")
	fmt.Scan(&firstName)
	fmt.Println("Enter your last name: ")
	fmt.Scan(&lastName)
	fmt.Println("Enter your email address: ")
	fmt.Scan(&email)
	fmt.Println("Enter number of tickets: ")
	fmt.Scan(&userTicket)

	return firstName, lastName, email, userTicket
}

func bookTicket(userTicket uint, firstName string, lastName string, email string) {
	remainingTicket = remainingTicket - userTicket

	// Create a map for a user
	var userData = UserData{
		firstName:       firstName,
		lastName:        lastName,
		email:           email,
		numbersOfTicket: userTicket,
	}

	bookings = append(bookings, userData)
	fmt.Printf("List of bookings is %v\n", bookings)

	fmt.Printf("Thank you %v %v for booking %v tickets. An confirmation email will be sent at %v.\n", firstName, lastName, userTicket, email)
	fmt.Printf("%v tickets remaining for %v.\n", remainingTicket, conferenceName)

}

func sendTicket(userTicket uint, firstName string, lastName string, email string) {
	time.Sleep(10 * time.Second)
	var ticket = fmt.Sprintf("%v tickets for %v %v", userTicket, firstName, lastName)
	fmt.Println("###########")
	fmt.Printf("Sending ticket %v to email address %v\n", ticket, email)
	fmt.Println("###########")
	wg.Done()
}
